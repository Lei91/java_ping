package com.main;

import java.awt.Canvas;
import java.awt.Dimension;

public class Game extends Canvas implements Runnable  {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -668240625892092763L;
	
	public static final int WIDTH = 1000;
	public static final int HEIGHT = WIDTH - 9/16;
	
	public boolean running = false;
	private Thread gameThread;  
	
	private Ball ball;
	private Paddle paddle1;
	private Paddle paddle2;

	public Game() {
		
		canvasSetup();
		new Window("Ping-Pong",this);
		
	}
	
	private void canvasSetup() {
		this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		this.setMaximumSize(new Dimension(WIDTH, HEIGHT));
		this.setMaximumSize(new Dimension(WIDTH, HEIGHT));
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

	public void start() {
		gameThread = new Thread(this);
		gameThread.start();
		running = true;
		
	}
	
	public void stop() {
		try {
			gameThread.join();
			running = false;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		new Game();
	}
}
